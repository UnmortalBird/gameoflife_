#ifndef MYDRUG_H
#define MYDRUG_H

#include <QWidget>
#include <QDrag>

#include <widgetmimedata.h>

class MyDrug : public QDrag {
public:
    MyDrug(QWidget * pwgtDragSource = 0) : QDrag(pwgtDragSource)    {}

    void setWidget(QWidget * pwgt)  {
        WidgetMimeData *pmd = new WidgetMimeData;
        pmd->setWidget(pwgt);
        setMimeData(pmd);
    }
};

#endif // MYDRUG_H
