#include "palettewidget.h"


paletteWidget::paletteWidget(size_t cellSize, QWidget *parent) :
    QWidget(parent, Qt::Window),
    m_cellSize(cellSize)
{
    setWindowTitle("Presets");

    setMinimumWidth(220);

    m_layout = new QGridLayout(this);
    this->setLayout(m_layout);

    parsePresetFiles();
}


paletteWidget::~paletteWidget() {
    for (int i = 0; i < m_layout->columnCount(); ++i)    {
        LifeGrid *preset = dynamic_cast<LifeGrid*>(m_layout->itemAtPosition(1, i)->widget());
        gridData *model = preset->model();
        delete model;
    }
}


/************************* Public interface **********************************/

void paletteWidget::repaintPresets(size_t cellSize)    {
    m_cellSize = cellSize;

    for (int i = 0; i< m_layout->columnCount(); ++i)    {
        QPushButton *btn = dynamic_cast<QPushButton*>(m_layout->itemAtPosition(0, i)->widget());
        LifeGrid *preset = dynamic_cast<LifeGrid*>(m_layout->itemAtPosition(1, i)->widget());

        preset->setSize(preset->rows(), preset->columns(), m_cellSize);

        QPixmap image(preset->size());
        preset->render(&image);
        btn->setIcon(image);
        btn->setIconSize(preset->size());
        btn->setFixedSize(QSize(image.width()+15, image.height()+15));

        setPaletteSize();
    }
}


/************************* Protected events **********************************/

bool paletteWidget::eventFilter(QObject *watched, QEvent *event)    {
    if(event->type()==QEvent::MouseButtonPress) {
        QPoint posInPushButton=dynamic_cast<QMouseEvent*>(event)->pos();
        QPoint posOfPushButton=dynamic_cast<QWidget*>(watched)->pos();
        m_DragPos = posInPushButton + posOfPushButton;

        if (dynamic_cast<QMouseEvent*>(event)->button() == Qt::RightButton)   {
            turnPreset();
        }
    }
    else if(event->type()==QEvent::MouseMove) {
        mouseMoveEvent(dynamic_cast<QMouseEvent*>(event));
    }

    return QWidget::eventFilter(watched, event);
}


void paletteWidget::mouseMoveEvent(QMouseEvent *event)  {
    QPushButton *sender = dynamic_cast<QPushButton*>(childAt(m_DragPos));

    if (!sender)
        return;

    int idx = m_layout->indexOf(sender);
    LifeGrid *preset = static_cast<LifeGrid*>(m_layout->itemAt(idx+1)->widget());

    if ((event->buttons() & Qt::LeftButton)
            && ((event->pos() - m_DragPos).manhattanLength() > QApplication::startDragDistance()))   {
        QPixmap image(preset->size()), targetImage(preset->size());
        preset->render(&image);
        targetImage.fill(Qt::transparent);

        QPainter painter(&targetImage);
        painter.setOpacity(0.5);
        painter.drawPixmap(0, 0, image);
        painter.end();

        MyDrug *pDrag = new MyDrug(this);
        pDrag->setPixmap(targetImage);
        pDrag->setWidget(preset);
        pDrag->exec(Qt::LinkAction);

        m_DragPos = QPoint(0,0);
    }
}


/************************* Private functions *********************************/

void paletteWidget::parsePresetFiles()  {
    QDir presetsDir(QDir::currentPath() + "/presets");
    QStringList fileList = presetsDir.entryList(QStringList("*.gol"), QDir::Files);

    for (int i = 0; i < fileList.size(); ++i) {
        QFile fileIn(presetsDir.filePath(fileList.at(i)));
        fileIn.open(QIODevice::ReadOnly);

        gridData *data = new gridData;
        QVector<size_t> subdata;
        QStringList lineData;

        while(!fileIn.atEnd())  {
            lineData = QString(fileIn.readLine()).split(" ");
            for(QString val : lineData)
                subdata.push_back(val.toUInt());
            data->push_back(subdata);

            subdata.erase(subdata.begin(), subdata.end());
        }

        LifeGrid * preset = initPreset(data);

        preset->hide();

        QPushButton *btn = new QPushButton(this);
        btn->setToolTip(fileList.at(i).section('.', 0, -2));

        QPixmap image(preset->size());
        preset->render(&image);
        btn->setIcon(image);
        btn->setIconSize(preset->size());
        btn->setFixedSize(QSize(image.width()+15, image.height()+15));

        QString qss = QString("QPushButton{border-radius: 10px; background-color: %1;}"
                              "QPushButton:hover{border-radius: 10px; border-color: #8f8f91; border-style: solid; border-width: 1px; }").arg(palette().color(QPalette::Window).name());

        btn->setStyleSheet(qss);

        m_layout->addWidget(btn, 0, i, Qt::AlignTop);
        m_layout->addWidget(preset, 1, i, Qt::AlignTop);
        btn->installEventFilter(this);

        setPaletteSize();
    }
}



LifeGrid * paletteWidget::initPreset(gridData *data)  {
    LifeGrid * preset = new LifeGrid(this);

    size_t rows = (*data)[0][0];
    size_t cols = (*data)[0][1];

    data->remove(0);

    preset->setSize(rows, cols, m_cellSize);
    preset->setModel(data);

    return preset;
}



void paletteWidget::setPaletteSize() {
    int h = 0, w = 0;
    for (int i = 0; i< m_layout->columnCount(); ++i)    {
        QPushButton *btn = dynamic_cast<QPushButton*>(m_layout->itemAtPosition(0, i)->widget());

        if (btn->height() > h)
            h = btn->height();
        w += btn->width() + 10;

        m_layout->setColumnMinimumWidth(i, btn->width());
        m_layout->setColumnStretch(i, 0);
    }
    setFixedSize(w, h + 25);
}


void paletteWidget::turnPreset()    {
    QPushButton *btn = dynamic_cast<QPushButton*>(childAt(m_DragPos));

    if (!btn)
        return;

    int idx = m_layout->indexOf(btn);
    LifeGrid *preset = static_cast<LifeGrid*>(m_layout->itemAt(idx+1)->widget());

    size_t newRows = preset->columns();
    size_t newCols = preset->rows();


    gridData *newVec = new gridData(newRows);
    gridData *oldVec = preset->model();

    for (size_t i = 0; i < newRows; i++)
        for (size_t j = 0; j < newCols; j++)
            (*newVec)[i].append((*oldVec)[newCols - 1 - j][i]);

    delete oldVec;

    preset->setSize(newRows, newCols, m_cellSize);
    preset->setModel(newVec);

    QPixmap image(preset->size());
    preset->render(&image);
    btn->setIcon(image);
    btn->setIconSize(preset->size());
    btn->setFixedSize(QSize(image.width()+15, image.height()+15));

    setPaletteSize();
}




