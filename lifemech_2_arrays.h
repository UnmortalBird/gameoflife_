#ifndef LIFEMECH_2_ARRAYS_H
#define LIFEMECH_2_ARRAYS_H

#include "ilifemechanism.h"
#include <QTime>
#define gridData QVector<QVector<size_t>>

//#include <future>
#include <windows.h>
#include <QtConcurrent/QtConcurrent>
#include <QVector>
#include <QPair>


class LifeMech_2_arrays : public ILifeMechanism {

public:
    explicit LifeMech_2_arrays(size_t rows, size_t cols);
    ~LifeMech_2_arrays();

    void        resizeArray(size_t rows, size_t cols)   override;
    void        resetData()                             override;
    void        resetData(gridData &data)               override;
    int         nextGen()                               override;
    void        invertCell(int row, int col)            override;
    void        setPreset(int pos_x, int pos_y, gridData &data) override;
    void        setRules(QString &rules)                override;
    gridData    *getState()                             override;

private:
    int         countNeighbors(int currRow, int currCol);
    void        updateCurGen();
    size_t      countCPUCores();

    void        mthreadNextGen(QPair<int, int> &startFin);
    size_t      mthreadModifyCell(int row, int col, int cntNeighbors);

    gridData    *m_currData;
    gridData    *m_nextData;

    QString     m_birthLiveRule = "3/23";
    int         m_rows = 0;
    int         m_cols = 0;
};

#endif // LIFEMECH_2_ARRAYS_H
