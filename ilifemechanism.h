#ifndef ILIFEMECHANISM_H
#define ILIFEMECHANISM_H

#include <QtCore>
#define gridData QVector<QVector<size_t>>

class ILifeMechanism    {
public:
    void virtual    resizeArray(size_t rows, size_t cols)   = 0;
    void virtual    resetData()                             = 0;
    void virtual    resetData(gridData &data)               = 0;
    int virtual     nextGen()                               = 0;
    void virtual    invertCell(int row, int col)            = 0;
    void virtual    setPreset(int pos_x, int pos_y, gridData &data) = 0;
    void virtual    setRules(QString& rules)                = 0;
    gridData virtual     *getState()                        = 0;
};

#endif // ILIFEMECHANISM_H
