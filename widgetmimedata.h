#ifndef WIDGETMIMEDATA_H
#define WIDGETMIMEDATA_H

#include <QMimeData>

class WidgetMimeData : public QMimeData
{
public:
    WidgetMimeData() : QMimeData()  {}
    virtual ~WidgetMimeData()   {}

    static QString mimeType()   {
        return "applcation/widget";
    }

    void setWidget(QWidget * pwgt)  {
        m_pwgt = pwgt;
        setData(mimeType(), QByteArray());
    }

    QWidget * widget() const    {
        return m_pwgt;
    }

private:
    QWidget * m_pwgt;
};

#endif // WIDGETMIMEDATA_H
