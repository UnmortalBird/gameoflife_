#include "lifegrid.h"


/************************* Public interface **********************************/

void LifeGrid::setSize(size_t rows, size_t cols, size_t cell_size)  {
    m_rows = rows;
    m_cols = cols;
    m_cellSize = cell_size;
    setFixedSize(m_cols*m_cellSize+1, m_rows*m_cellSize+1);

}


void LifeGrid::setModel(gridData * data) {
    Q_ASSERT(data->size() == m_rows);
    Q_ASSERT(data->at(0).size() == m_cols);

    if (data->size() != m_rows)
        return;

    for (QVector<size_t> val : *data)   {
        if (val.size() != m_cols)
            return;
    }

    m_data = data;
}



/************************* Protected events **********************************/

void LifeGrid::paintEvent(QPaintEvent *event)    {
    Q_UNUSED(event);

    QPainter qp(this);

    qp.setPen(QPen(Qt::gray, 1));
    for (int i = 0; i <= (m_cols * m_cellSize); i += m_cellSize)   {
        qp.drawLine(QPoint(i, 0), QPoint(i, m_rows * m_cellSize));
    }
    for (int i = 0; i <= (m_rows * m_cellSize); i += m_cellSize)   {
        qp.drawLine(QPoint(0, i), QPoint(m_cols * m_cellSize, i));
    }

    qp.setPen(QPen(Qt::black, 1));
    qp.setBrush(Qt::black);
    for (int i = 0; i < m_rows; i++)   {
        for (int j = 0; j < m_cols; j++)   {
            if((*m_data)[i][j])
                qp.drawRect(j * m_cellSize, i * m_cellSize, m_cellSize, m_cellSize);
        }
    }

    if (m_dragEvent)  {
        qp.setPen(QPen(Qt::red, 1));
        qp.setBrush(Qt::red);

        QPoint topLeft(m_dropPos);
        QPoint topRight(m_dropPos.x() + m_presetWidth, m_dropPos.y() );
        QPoint bottomLeft(m_dropPos.x(), m_dropPos.y() + m_presetHeight);
        QPoint bottomRight(m_dropPos.x() + m_presetWidth, m_dropPos.y() + m_presetHeight );

        qp.drawLine(topLeft, topRight);
        qp.drawLine(topRight, bottomRight);
        qp.drawLine(bottomRight, bottomLeft);
        qp.drawLine(bottomLeft, topLeft);
    }
}



void LifeGrid::mousePressEvent(QMouseEvent *event)  {
    if (event->button() == Qt::LeftButton)
        emit clicked(event->pos());
}


void LifeGrid::dragEnterEvent(QDragEnterEvent *event)   {
    if (event->mimeData()->hasFormat(WidgetMimeData::mimeType()))   {
        event->acceptProposedAction();

        m_dragEvent = true;

        const WidgetMimeData * pmmd = dynamic_cast<const WidgetMimeData*>(event->mimeData());

//        if (!pmmd)
//            return;
        LifeGrid * preset = static_cast<LifeGrid*>(pmmd->widget());

        m_presetHeight = preset->rows() * m_cellSize;
        m_presetWidth = preset->columns() * m_cellSize;
    }
}


void LifeGrid::dragLeaveEvent(QDragLeaveEvent *event)   {
    Q_UNUSED(event);
    m_dragEvent = false;
    repaint();
}


void LifeGrid::dragMoveEvent(QDragMoveEvent *event)  {
    m_dropPos = QPoint((event->pos().x()/m_cellSize)*m_cellSize, (event->pos().y()/m_cellSize)*m_cellSize);
    repaint();
}


void LifeGrid::dropEvent(QDropEvent *event) {
    const WidgetMimeData * pmmd = dynamic_cast<const WidgetMimeData*>(event->mimeData());

    if (!pmmd)
        return;

    emit presetDroped(event->pos(), pmmd->widget());

    m_dragEvent = false;
    repaint();
}

