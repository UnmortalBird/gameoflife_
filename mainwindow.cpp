#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Game of Life");

    ui->gridWidget->setAcceptDrops(true);
    ui->gridWidget->setSize(m_rows, m_cols, m_cellSize);
    setFixedSize(centralWidget()->sizeHint());

    setLineEdits();

    lifeMech = new LifeMech_2_arrays(m_rows, m_cols);
    lifeMech->resetData();
    updateViewGrid();

    ui->labelItersCnt->setText(QString::number(m_itersCnt));
    ui->labelTimerCnt->setText("00:00.000");

    connect(ui->startButton,    &QPushButton::clicked,      this,   &MainWindow::startEvolution );
    connect(ui->stopButton,     &QPushButton::clicked,      this,   &MainWindow::stopEvolution  );
    connect(ui->stepButton,     &QPushButton::clicked,      this,   &MainWindow::nextGen        );
    connect(ui->resetButton,    &QPushButton::clicked,      this,   &MainWindow::reset          );
    connect(ui->gridWidget,     &LifeGrid::clicked,         this,   &MainWindow::setCell        );
    connect(ui->gridWidget,     &LifeGrid::presetDroped,    this,   &MainWindow::sendPreset     );
    connect(ui->btnApply,       &QPushButton::clicked,      this,   &MainWindow::setParams      );
    connect(ui->btnPresets,     &QPushButton::clicked,      this,   &MainWindow::createPalette  );

    connect(ui->leditCellSize,  &QLineEdit::returnPressed,  this,   &MainWindow::setParams      );
    connect(ui->leditRows,      &QLineEdit::returnPressed,  this,   &MainWindow::setParams      );
    connect(ui->leditCols,      &QLineEdit::returnPressed,  this,   &MainWindow::setParams      );
    connect(ui->leditDelay,     &QLineEdit::returnPressed,  this,   &MainWindow::setParams      );
    connect(ui->leditRule,      &QLineEdit::returnPressed,  this,   &MainWindow::setParams      );

    connect(ui->radioEmpty,     &QRadioButton::clicked,     this,   &MainWindow::reset          );
    connect(ui->radioRandom,    &QRadioButton::clicked,     this,   &MainWindow::reset          );
}


MainWindow::~MainWindow()   {
    delete lifeMech;
    delete ui;
}


void MainWindow::timerEvent(QTimerEvent *e)  {
    Q_UNUSED(e);

    nextGen();
//    qDebug() << "total fin";
}


void MainWindow::setLineEdits()    {
    ui->leditCellSize->setText(QString::number(m_cellSize));
    ui->leditRows->setText(QString::number(m_rows));
    ui->leditCols->setText(QString::number(m_cols));
    ui->leditDelay->setText(QString::number(m_timerDelay));
    ui->leditRule->setText(m_birthLiveRule);

    ui->leditCellSize->setValidator(new QRegExpValidator(QRegExp("\\d?\\d?"), this));
    ui->leditRows->setValidator(new QRegExpValidator(QRegExp("\\d?\\d?\\d?\\d?"), this));
    ui->leditCols->setValidator(new QRegExpValidator(QRegExp("\\d?\\d?\\d?\\d?"), this));
    ui->leditDelay->setValidator(new QRegExpValidator(QRegExp("\\d?\\d?\\d?\\d?"), this));
    ui->leditRule->setValidator(new QRegExpValidator(QRegExp("\\d/\\d+"), this));
}


void MainWindow::startEvolution()   {
    m_currTimer.restart();
    m_timerID = startTimer(m_timerDelay);
    ui->startButton->setEnabled(false);
}


void MainWindow::stopEvolution()    {
    if (!m_timerID)
        return;

    m_timerCnt += m_currTimer.elapsed();

    killTimer(m_timerID);
    m_timerID = 0;
    ui->startButton->setEnabled(true);

}


void MainWindow::reset()    {
    stopEvolution();
    setArray();

    m_itersCnt = 0;
    m_timerCnt = 0;
    ui->labelItersCnt->setText(QString::number(m_itersCnt));
    ui->labelTimerCnt->setText("00:00.000");
}


void MainWindow::setParams()   {
    stopEvolution();

    m_cellSize       = ui->leditCellSize->text().toInt();
    m_rows           = ui->leditRows->text().toInt();
    m_cols           = ui->leditCols->text().toInt();
    m_timerDelay     = ui->leditDelay->text().toInt();
    m_birthLiveRule  = ui->leditRule->text();

    ui->gridWidget->setSize(m_rows, m_cols, m_cellSize);
    setFixedSize(centralWidget()->sizeHint());

    lifeMech->resizeArray(m_rows, m_cols);
    lifeMech->setRules(m_birthLiveRule);
    updateViewGrid();
    updatePalette();
}


void MainWindow::setArray() {
    if (ui->radioRandom->isChecked())
        setRandomArray();
    else
        lifeMech->resetData();

    updateViewGrid();
}


void MainWindow::setRandomArray()   {
    QVector<size_t> tmpVec(m_cols);
    gridData vec = gridData(m_rows, tmpVec);

    QTime time = QTime::currentTime();
    qsrand((uint) time.msec());

    for (int i = 0; i < m_rows; i++)   {
        for (int j = 0; j < m_cols; j++)   {
            vec[i][j] = qrand() % 2;
        }
    }

    lifeMech->resetData(vec);
}


void MainWindow::setCell(const QPoint &pos)  {
    int row = pos.y() / m_cellSize;
    int col = pos.x() / m_cellSize;
    lifeMech->invertCell(row, col);
    updateViewGrid();
}


void MainWindow::sendPreset(const QPoint &pos, QWidget *pwgt) {

    LifeGrid * preset = static_cast<LifeGrid*>(pwgt);
    size_t pos_x = pos.x() / m_cellSize;
    size_t pos_y = pos.y() / m_cellSize;

    gridData *data = preset->model();
    lifeMech->setPreset(pos_x, pos_y, *data);
    updateViewGrid();
}


void MainWindow::createPalette()    {
    if (!m_palette)  {
        QDir presetsDir(QDir::currentPath() + "/presets", "*.gol");
        if (presetsDir.isEmpty())
            return;

        m_palette = new paletteWidget(m_cellSize, this);
        m_palette->setAttribute(Qt::WA_DeleteOnClose);
        m_palette->show();
    }
}

void MainWindow::updatePalette()    {
    if (m_palette)
        m_palette->repaintPresets(m_cellSize);
}



void MainWindow::updateViewGrid()    {
    ui->gridWidget->setModel(lifeMech->getState());
    ui->gridWidget->repaint();
}



void MainWindow::nextGen() {
    if(lifeMech->nextGen() < 0)   {
        stopEvolution();
        return;
    }

    m_itersCnt++;
    ui->labelItersCnt->setText(QString::number(m_itersCnt));

    int it = m_timerCnt + m_currTimer.elapsed();
    QTime t(0, it/60000, (it%60000)/1000, it%1000);
    ui->labelTimerCnt->setText(t.toString("mm:ss.zzz"));

    updateViewGrid();
}

