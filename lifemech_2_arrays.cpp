#include "lifemech_2_arrays.h"

LifeMech_2_arrays::LifeMech_2_arrays(size_t rows, size_t cols)  :
    m_rows(rows), m_cols(cols)
{
    QVector<size_t> tmpVec(m_cols, 0);
    m_currData = new gridData(m_rows, tmpVec);
    m_nextData = new gridData(m_rows, tmpVec);
}

LifeMech_2_arrays::~LifeMech_2_arrays() {
    delete m_currData;
    delete m_nextData;
}


/************************* Public interface **********************************/

void LifeMech_2_arrays::resizeArray(size_t rows, size_t cols)  {
    m_rows = rows;
    m_cols = cols;

    m_currData->resize(m_rows);
    m_nextData->resize(m_rows);
    for (QVector<size_t> &val : *m_currData)
        val.resize(m_cols);
    for (QVector<size_t> &val : *m_nextData)
        val.resize(m_cols);

    (*m_currData).squeeze();
    (*m_nextData).squeeze();
}


void LifeMech_2_arrays::resetData() {
    QVector<size_t> tmpVec(m_cols, 0);
    for (QVector<size_t> &vec : (*m_currData))
        vec = tmpVec;
}


void LifeMech_2_arrays::resetData(gridData &data)  {
    if (data.size() != m_currData->size())
        return;

    for (int i = 0; i < data.size(); ++i) {
        if ((*m_currData)[i].size() != data[i].size())
            return;
        (*m_currData)[i] = data[i];
    }
}


int LifeMech_2_arrays::nextGen()  {
    QVector< QPair<int, int> > v;
    const int threads = QThread::idealThreadCount();    //     countCPUCores();

    for (int i = 0; i < threads; ++i) {
        if (i < (threads-1))
            v << QPair<int, int>(i*(m_rows/threads), (i+1)*(m_rows/threads)-1);
        else
            v << QPair<int, int>(i*(m_rows/threads), m_rows-1);
    }

    QFuture<void> fut = QtConcurrent::map(v, [this] (QPair<int, int> &startFin) {   mthreadNextGen(startFin);   });
    fut.waitForFinished();

    if (*m_currData == *m_nextData)
        return -1;

    updateCurGen();
    return 1;
}


void LifeMech_2_arrays::invertCell(int row, int col)   {
    (*m_currData)[row][col] = (*m_currData)[row][col] ? 0 : 1;
}


void LifeMech_2_arrays::setPreset(int pos_x, int pos_y, gridData &data) {
    int rows = data.size();
    int cols = data[0].size();

    for (int i = 0; (i < rows) && (pos_y + i < m_rows); i++)   {
        for (int j = 0; (j < cols) && (pos_x + j < m_cols); j++)
            (*m_currData)[pos_y+i][pos_x+j] = data[i][j];
    }
}


void LifeMech_2_arrays::setRules(QString & rules) {
    m_birthLiveRule = rules;
}


gridData * LifeMech_2_arrays::getState() {
    return m_currData;
}

/************************* Private functions *********************************/

int LifeMech_2_arrays::countNeighbors(int currRow, int currCol)  {
    int cnt = 0;
    for (int m = currRow-1; m <= currRow+1; m++)   {
        for (int n = currCol-1; n <= currCol+1; n++)   {
            if ((m == currRow) && (n == currCol))
                continue;
            int m_ = (m >= 0) ? m % m_rows : m_rows + m;
            int n_ = (n >= 0) ? n % m_cols : m_cols + n;
            cnt += (*m_currData)[m_][n_];
        }
    }

    return cnt;
}


void LifeMech_2_arrays::updateCurGen()  {
    gridData *tmpData = m_nextData;
    m_nextData = m_currData;
    m_currData = tmpData;
}


void LifeMech_2_arrays::mthreadNextGen(QPair<int, int> &startFin)   {
    int cnt=0;

    for (int i = startFin.first; i <= startFin.second; i++)   {
        for (int j = 0; j < m_cols; j++)   {
            cnt = countNeighbors(i, j);
            size_t live = mthreadModifyCell(i, j, cnt);
            (*m_nextData)[i][j] = live;
        }
    }
}


size_t LifeMech_2_arrays::mthreadModifyCell(int row, int col, int cntNeighbors)   {
    QString birthStr = m_birthLiveRule.left(m_birthLiveRule.indexOf("/"));
    QString liveStr  = m_birthLiveRule.right(m_birthLiveRule.length() - m_birthLiveRule.indexOf("/") - 1);

    size_t live = 0;
    if ((*m_currData)[row][col])   {
        if (liveStr.contains(QString::number(cntNeighbors)))
            live = 1;
    }   else    {
        if (birthStr.contains(QString::number(cntNeighbors)))
            live = 1;
    }

    return live;
}


size_t LifeMech_2_arrays::countCPUCores()    {
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    size_t logicCPUCores = sysinfo.dwNumberOfProcessors;
    size_t physicCPUCores;
    uint32_t registers[4];

    __asm__ __volatile__ ("cpuid " :
                          "=a" (registers[0]),
                          "=b" (registers[1]),
                          "=c" (registers[2]),
                          "=d" (registers[3])
                          : "a" (1), "c" (0));

    unsigned CPUFeatureSet = registers[3];
    bool hyperthreading = CPUFeatureSet & (1 << 28);

    if (hyperthreading){
        physicCPUCores = logicCPUCores / 2;
    } else {
        physicCPUCores = logicCPUCores;
    }

    return physicCPUCores;
}
