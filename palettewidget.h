#ifndef PALETTEWIDGET_H
#define PALETTEWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QDir>

#include <QApplication>
#include <QPainter>
#include <QPushButton>

#include "lifegrid.h"
#include "mydrug.h"

#define gridData QVector<QVector<size_t>>

class paletteWidget : public QWidget    {
    Q_OBJECT
public:
    explicit paletteWidget(size_t cellSize, QWidget *parent = nullptr);
    ~paletteWidget();

    void    repaintPresets(size_t cellSize);

protected:
    bool    eventFilter(QObject *watched, QEvent *event);
    void    mouseMoveEvent(QMouseEvent* e);

private:
    void        parsePresetFiles();
    LifeGrid    *initPreset(gridData *);
    void        setPaletteSize();
    void        turnPreset();

    QPoint      m_DragPos;
    int         m_cellSize;

    QGridLayout *m_layout;
};

#endif // PALETTEWIDGET_H
