#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include <QRegExpValidator>
#include <QDebug>

#include "lifegrid.h"
#include "lifemech_2_arrays.h"
#include "palettewidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow   {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void    timerEvent(QTimerEvent *e);

private:
    void    setLineEdits();
    void    setParams();
    void    updateViewGrid();
    void    setArray();
    void    setRandomArray();
    void    nextGen();
    void    startEvolution();
    void    stopEvolution();
    void    reset();
    void    setCell(const QPoint &pos);
    void    sendPreset(const QPoint &pos, QWidget *pwgt);

    int     m_cellSize      = 10;
    int     m_rows          = 20;
    int     m_cols          = 20;
    int     m_timerDelay    = 10;
    int     m_typeID        = 0;
    QString m_birthLiveRule = "3/23";

    int     m_timerID       = 0;

    int     m_timerCnt      = 0;
    QTime   m_currTimer;

    int     m_itersCnt      = 0;

    void    createPalette();
    void    updatePalette();
    QPointer<paletteWidget>  m_palette;

    ILifeMechanism *lifeMech;

    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
