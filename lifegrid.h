#ifndef LIFEGRID_H
#define LIFEGRID_H

#include <QWidget>
#include <QPainter>

#include <QMouseEvent>
#include <QMimeData>
#include <QDebug>

#include "widgetmimedata.h"

#define gridData QVector<QVector<size_t>>


class LifeGrid : public QWidget
{
    Q_OBJECT
public:
    explicit LifeGrid(QWidget *parent = nullptr)  : QWidget(parent)  {}

signals:
    void    clicked(const QPoint &pos);
    void    presetDroped(const QPoint &pos, QWidget *pwgt);

public slots:
    void        setSize(size_t rows, size_t cols, size_t cell_size);
    void        setModel(gridData * data);
    int         rows()      {   return m_rows;  }
    int         columns()   {   return m_cols;  }
    gridData    *model()    {   return m_data;  }


protected:
    void    paintEvent(QPaintEvent *);
    void    mousePressEvent(QMouseEvent *);
    void    dragEnterEvent(QDragEnterEvent *);
    void    dragLeaveEvent(QDragLeaveEvent *);
    void    dragMoveEvent(QDragMoveEvent *);
    void    dropEvent(QDropEvent *);

private:
    int     m_cellSize   = 10;
    int     m_rows       = 0;
    int     m_cols       = 0;

    QPoint  m_dropPos;
    bool    m_dragEvent = false;
    int     m_presetHeight, m_presetWidth;

    QVector< QVector<size_t> > *m_data;
};

#endif // LIFEGRID_H
